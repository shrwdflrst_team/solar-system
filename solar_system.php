<?php
/**
 * INSTRUCTIONS
 * Your task is to code a working model of a simplified version of our solar system.
 * Throughout this file and the corresponding html file you'll find several sections
 * of code marked TODO, fill in these sections of code to power the visualisation on
 * the html page.  The html page utilises the HTML5 canvas element. If you complete
 * the task you should be able to generate an animated version of the solar system
 * showing the orbits over time.
 *
 * You should ONLY need to edit sections marked 'TODO'
 */


/**
 * TODO add classes for Sun, Planet and Moon that can be used by the calling code
 * at the bottom of this file.  Choose whichever object representation you feel
 * is appropriate but be aware the calling code requires several specifically named
 * methods, you should write these methods rather than editing the calling code.
 */

class PolarCoordinate
{
	public $radius = null;
	public $azimuth = null;

	public function __construct($radius, $azimuth)
	{
		$this->radius = $radius;
		$this->azimuth = $azimuth;
	}

	/**
	 * Transform this coordinate into a cartesian form.
	 */
	public function asCartesian()
	{
		// TODO Calculate the x and y position as a cartesian
		$x = $this->radius * cos($this->azimuth); // REPLACE 0 with your calcualtion
		$y = $this->radius * sin($this->azimuth); // REPLACE 0 with your calcualtion

		return new CartesianCoordinate($x, $y);
	}
}

class CartesianCoordinate
{
	public $x = null;
	public $y = null;

	public function __construct($x, $y)
	{
		$this->x = $x;
		$this->y = $y;
	}

	public function addOffset(CartesianCoordinate $offset)
	{
		$this->x += $offset->x;
		$this->y += $offset->y;

		return $this;
	}
}


abstract class Renderer {
	public abstract function draw($orbital_body, $day);

	public function drawIncludingSatellites(OrbitalBody $orbital_body, $day)
	{
		// TODO - draw the positions of children of the object using the draw function of the sub class
        $positions[] = $this->draw($orbital_body, $day);

        foreach($orbital_body->getSatellites() as $satellite) {
            $positions[] = $this->drawIncludingSatellites($satellite, $day);
        }

		return implode(",", $positions);
	}
}

class CartesianListRenderer extends Renderer {
	public function draw($orbital_body, $day) {
		$coord = $orbital_body->cartesianPositionRelativeToSun($day);
		echo "<p>{$orbital_body->name} - x: {$coord->x}, y: {$coord->y} </p>\r\n";
	}
}

class JsonRenderer extends Renderer {
	public function draw($orbital_body, $day)
	{
		$coord = $orbital_body->cartesianPositionRelativeToSun($day);

		$parentX = 0;
		$parentY = 0;
		if ($orbital_body->orbits)
		{
			$parent_coord = $orbital_body->orbits->cartesianPositionRelativeToSun($day);
			$parentX = $parent_coord->x;
			$parentY = $parent_coord->y;
		}

		return "{name:'{$orbital_body->name}',x:{$coord->x},y:{$coord->y},radius:{$orbital_body->radius}, orbit_radius:{$orbital_body->orbital_radius}, orbit_centre_x:{$parentX}, orbit_centre_y:{$parentY}}";
	}
}

class SolarPositionList {
	public static function draw(OrbitalBody $sun, $day, Renderer $renderer)
	{
		return 	$renderer->drawIncludingSatellites($sun, $day);
	}
}

/**
 * The following objects are the only ones you're expected to use,
 * ignore other elements of the solar system
 */


class OrbitalBody {

    public
        $name = "",
        $orbital_radius,
        $orbits = null,
        $radius;

    protected
        $satellites = array(),
        $orbital_period,
        $mass;

    /**
     * __construct
     * @param string $name The name of the body.
     * @param object $orbits The body this body orbits.
     * @param float $orbital_radius The distance at which this body orbits it's parent, measured in AU multiples of Earths orbital radius from the sun.
     * @param float $orbital_period Number of earth days required to complete an orbit
     * @param float $initial_radians The number of radians through it's orbit the body is on day 0.
     * @param float $mass The mass of the body in multiples of the weight of Earth.
     * @param float $radius The radius of the body in multiples of earths radius
     */
    public function __construct(
        $name,
        $orbits,
        $orbital_radius,
        $orbital_period,
        $initial_radians,
        $mass,
        $radius
    )
    {
        $this->name             = $name;
        $this->orbits           = $orbits;
        $this->orbital_radius   = (float) $orbital_radius;
        $this->orbital_period   = (float) $orbital_period;
        $this->initial_radians  = (float) $initial_radians;
        $this->mass             = (float) $mass;
        $this->radius           = (float) $radius;
    }


    public function addSatellite(OrbitalBody $body)
    {
        $this->satellites[] = $body;
    }


    public function getSatellites()
    {
        return $this->satellites;
    }

    public function cartesianPositionRelativeToSun($day)
    {
        if($this->orbits) {
            $degrees = ($day / $this->orbital_period) * 360;
            $radians = ($degrees * M_PI / 180);
            $radians += $this->initial_radians;
            $polar = new PolarCoordinate($this->orbital_radius, $radians);
            $coord = $polar->asCartesian();
            $parent_coord = $this->orbits->cartesianPositionRelativeToSun($day);
            $coord = new CartesianCoordinate($parent_coord->x + $coord->x, $parent_coord->y + $coord->y);
        }
        else {
            $coord = new CartesianCoordinate(0, 0);
        }
        return $coord;
    }

}

class Star extends OrbitalBody {}
class Planet extends OrbitalBody {}
class Moon extends OrbitalBody {}

$sun = new Star("Sol", null, 0, 0, 0, 332900, 55);
$mercury = new Planet("Mercury", $sun, 0.39, 87.9, 0, 0.055, 0.383);
$venus = new Planet("Venus", $sun, 0.72, 224, 3.14, 0.815, 0.949);
$earth = new Planet("Earth", $sun, 1, 365, 0, 1, 1);
$moon = new Moon("The Moon", $earth, 0.15, 28, 0, 0.0123, 0.0273); 	// Note the moons orbital_radius is heavily exaggerated.
$mars = new Planet("Mars", $sun, 1.52, 687, 0, 0.107, 0.533);
$jupiter = new Planet("Jupiter", $sun, 5.2, 4332, 0, 318, 11.209);


$sun->addSatellite($mercury);
$sun->addSatellite($venus);
$sun->addSatellite($earth);
$earth->addSatellite($moon);
$sun->addSatellite($mars);
$sun->addSatellite($jupiter);

$cartesianListRenderer = new CartesianListRenderer();
$jsonRenderer = new JsonRenderer();

$days = isset($_GET['days']) ? $_GET['days'] : 0;

//SolarPositionList::draw($sun, $days, $cartesianListRenderer);
echo "[".SolarPositionList::draw($sun, $days, $jsonRenderer)."]";

?>